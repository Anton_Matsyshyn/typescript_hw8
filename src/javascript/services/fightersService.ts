import { callApi } from '../helpers/apiHelper';
import { FighterInfo } from '../models/FighterInfo';

export async function getFighters():Promise<FighterInfo[]> {
  try {
    const endpoint = 'fighters.json';
    const apiResult = await callApi(endpoint, 'GET');
    
    return <FighterInfo[]>apiResult;
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id: string):Promise<FighterInfo> {
  try {
    const endpoint = `details/fighter/${id}.json`;
    const apiResult = await callApi(endpoint, 'GET');

    return <FighterInfo>apiResult;
  } catch(error) {
    throw error;
  }
}

