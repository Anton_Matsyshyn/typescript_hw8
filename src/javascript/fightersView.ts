import { createFighter, onFight, changeOnFightState, onShowModal, changeOnShowModalState } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import { Fighter } from './models/Fighter';
import { FighterInfo } from './models/FighterInfo';
import { getFighterDetails } from './services/fightersService';

export function createFighters(fighters:FighterInfo[]) {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements = fighters.map(fighter => createFighter(<Fighter>fighter, showFighterDetails, selectFighterForBattle));
  const fightersContainer = createElement({ tagName: 'div', className: 'fighters', attributes: {} });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

const fightersDetailsCache = new Map();

async function showFighterDetails(event: Event, fighter:Fighter):Promise<void> {
  if(onShowModal)
    return;

  changeOnShowModalState();

  const fullInfo = await getFighterInfo(fighter._id);
  showFighterDetailsModal(fullInfo);

  changeOnShowModalState();
}

export async function getFighterInfo(fighterId: string):Promise<FighterInfo> {
  return await getFighterDetails(fighterId);
}

function createFightersSelector() {
  const selectedFighters = new Map<string, FighterInfo>();

  return async function selectFighterForBattle(event:any, fighter: Fighter):Promise<void> {
    if(onFight)
      return;

    changeOnFightState();

    const fullInfo = await getFighterInfo(fighter._id);

    if (event.target.checked) {
      selectedFighters.set(fighter._id, fullInfo);
    } else { 
      selectedFighters.delete(fighter._id);
    }

    if (selectedFighters.size === 2) {
      const mapValues = selectedFighters.values();
      const winner = fight(mapValues.next().value, mapValues.next().value);
      showWinnerModal(winner);
    }
    
    changeOnFightState();
  }
}
