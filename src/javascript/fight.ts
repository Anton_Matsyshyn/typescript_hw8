import { Fighter } from "./models/Fighter";
import { FighterInfo } from "./models/FighterInfo";

export function fight(firstFighter: FighterInfo, secondFighter: FighterInfo): Fighter {
  const firstFighterHealth = firstFighter.health;
  const secondFighterHealth = secondFighter.health;

  const winner = makeFight(firstFighter, secondFighter);

  firstFighter.health = firstFighterHealth;
  secondFighter.health = secondFighterHealth;

  return winner;
}

function makeFight(firstFighter: FighterInfo, secondFighter: FighterInfo): Fighter {
  secondFighter.health = <number>secondFighter.health - getDamage(firstFighter, secondFighter);

  if(secondFighter.health <= 0)
    return <Fighter>firstFighter;

  firstFighter.health = <number>firstFighter.health - getDamage(secondFighter, firstFighter);

  if(firstFighter.health <= 0)
    return <Fighter>secondFighter;

  return makeFight(firstFighter, secondFighter);
}

export function getDamage(attacker: FighterInfo, enemy: FighterInfo): number {
  const damage = getHitPower(attacker) - getBlockPower(enemy);
  return damage >= 0 ? damage : 0;
}

export function getHitPower(fighter: FighterInfo): number {
  const criticalHitChance = Math.random() + 1;
  const power = <number>fighter.attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter: FighterInfo): number {
  const dodgeChance = Math.random() + 1;
  const power = <number>fighter.defense * dodgeChance;
  return power;
}
