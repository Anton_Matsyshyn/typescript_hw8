import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import { Fighter } from '../models/Fighter';
import { FighterInfo } from '../models/FighterInfo';

export  function showFighterDetailsModal(fighterDetail:FighterInfo) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighterDetail);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter: FighterInfo) {
  const { name, health, source, attack, defense } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body', attributes : {}});

  const imageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes: { src: source }});
  const nameElement = createElement({ tagName: 'span', className: 'fighter-details', attributes : {}});
  const healthElement = createElement({ tagName: 'span', className: 'fighter-details', attributes : {}});
  const attackElement = createElement({ tagName: 'span', className: 'fighter-details', attributes : {}});
  const defenseElement = createElement({ tagName: 'span', className: 'fighter-details', attributes : {}});
  // show fighter name, attack, defense, health, image

  nameElement.innerText = name;
  healthElement.innerText = `Health - ${health}`;
  attackElement.innerText = `Attack - ${attack}`;
  defenseElement.innerText = `Defense - ${defense}`;

  fighterDetails.append(imageElement);
  fighterDetails.append(nameElement);
  fighterDetails.append(healthElement);
  fighterDetails.append(attackElement);
  fighterDetails.append(defenseElement);
  
  return fighterDetails;
}
