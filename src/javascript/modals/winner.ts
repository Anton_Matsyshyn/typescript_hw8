import { Fighter } from "../models/Fighter";
import { showModal } from "./modal";
import { createElement } from "../helpers/domHelper";

export  function showWinnerModal(fighter: Fighter): void {
  const title = 'Winner';
  const bodyElement = createWinnerInfo(fighter);
  showModal({ title, bodyElement });
}

function createWinnerInfo(winner: Fighter): string | Node {
  const { name, source } = winner;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body', attributes : {}});

  const imageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes: { src: source }});
  const nameElement = createElement({ tagName: 'span', className: 'fighter-details', attributes : {}});

  nameElement.innerText = name;
  
  fighterDetails.append(imageElement);
  fighterDetails.append(nameElement);

  return fighterDetails;
}