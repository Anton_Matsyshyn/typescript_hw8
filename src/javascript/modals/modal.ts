import { createElement } from '../helpers/domHelper';

export function showModal({ title, bodyElement }: {title: string, bodyElement: Node|string }):void {
  const root = getModalContainer();
  const modal = createModal(title, bodyElement); 
  
  root.append(modal);
}

function getModalContainer() : HTMLElement {
  return <HTMLElement>document.getElementById('root');
}

function createModal(title: string, bodyElement: Node|string) : HTMLElement {
  const layer = createElement({ tagName: 'div', className: 'modal-layer', attributes : {}});
  const modalContainer = createElement({ tagName: 'div', className: 'modal-root', attributes : {}});
  const header = createHeader(title);

  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);

  return layer;
}

function createHeader(title: string) : HTMLElement {
  const headerElement = createElement({ tagName: 'div', className: 'modal-header', attributes : {} });
  const titleElement = createElement({ tagName: 'span', className: '', attributes : {} });
  const closeButton = createElement({ tagName: 'div', className: 'close-btn', attributes : {} });
  
  titleElement.innerText = title;
  closeButton.innerText = '×';
  closeButton.addEventListener('click', hideModal);
  headerElement.append(title, closeButton);
  
  return headerElement;
}

function hideModal(event: MouseEvent) : void {
  const modal = document.getElementsByClassName('modal-layer')[0];
  modal.remove();
}
